# APKSORTSEARCH

## History

Vai ser em português mesmo!
Quando descobri o Alpine linux à cerca de 1 (um) ano atrás
tive algumas dificuldades com seu uso, nada que fosse impossível,
pelo contrário, desafios que me ensinou muito a respeito do uso
do sistema.

Uma das coisas que mais me incomodou foi com o gerenciador de
pacotes do sistema o APK (Alpine Package Keeper), especificamente
na busca por pacotes nos repositórios. Ao executar o comando de 
busca, a maneira com que os resultados eram exibidos não ficava 
agradável aos meus olhos. Para resolver esse problema criei esse
pequeno script em Shell Script que pega os resultados e ordena 
alfabeticamente.


Quando criei esse script ele me servia, e os resultados eram
como eu esperavam que seriam. Mas hoje em dia esse script se
torna obsoleto, visto que o APK ja traz os resultados de maneira
ordenada.
